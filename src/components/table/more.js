import React, { Component, Fragment } from 'react';
import Popup from 'arui-feather/popup';
import Button from 'arui-feather/button';
import GridCol from 'arui-feather/grid-col/grid-col';
import IconButton from 'arui-feather/icon-button/icon-button';

class More extends Component {
  constructor() {
    super();
    this.state = {
      popup1: false
    };
    this.popup1 = this.popup1.bind(this);
    this.target1 = this.target1.bind(this);
  }
  popup1() {}
  target1() {}

  handleClick = (e) => {
    e.preventDefault();
    const { popup1 } = this.state;
    this.setState({ popup1: !popup1 });
  };

  componentDidMount() {
    this.popup1.setTarget(this.target1.control);
  }
  render() {
    return (
      <Fragment>
        <IconButton
          ref={(target) => {
            this.target1 = target;
          }}
          size="s"
          onClick={this.handleClick}>
          <span style={{ fontSize: '1.4em' }}>&#8285;</span>
        </IconButton>
        <Popup
          directions={['bottom-right']}
          size="m"
          type="tooltip"
          ref={(popup) => {
            this.popup1 = popup;
          }}
          visible={this.state.popup1}>
          <GridCol>
            <Button className="styled-btn" size="s" width="available">
              Корректировать
            </Button>
          </GridCol>
          <br />
          <GridCol>
            <Button className="styled-btn" size="s" width="available">
              Удалить
            </Button>
          </GridCol>
        </Popup>
      </Fragment>
    );
  }
}

export default More;
