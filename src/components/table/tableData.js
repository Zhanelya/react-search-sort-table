import React, { Fragment } from 'react';
import More from './more';
import { Table, Th, Td } from './style';

const TableData = (props) => {
  const { sort, tableProp, sorted, filtered } = props;

  return (
    <div>
      <Table>
        <thead>
          <tr>
            {tableProp.map((val, i) => (
              <Th
                key={i}
                onClick={() => sort(val.type)}
                className="cur-p"
                index={`${i}`}
                visibility={`${val.visibility}`}>
                {val.name}
                <span style={{ margin: '0 .2em' }}>
                  {sorted ? <span>&#8593;</span> : <span>&#8595;</span>}
                </span>
              </Th>
            ))}
            <th />
          </tr>
        </thead>
        <tbody>
          {filtered.length > 0 ? (
            filtered.map((val, i) => (
              <tr key={i}>
                {tableProp.map((prop) => (
                  <Fragment>
                    <Td className="cur-p">{val[prop.type]}</Td>
                  </Fragment>
                ))}
                <Td
                  onClick={(e) => {
                    e.preventDefault();
                  }}
                  className="ta-c">
                  <More />
                </Td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="9" className="ta-c">
                По вашему запросу ничего не найдено
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    </div>
  );
};

export default TableData;
