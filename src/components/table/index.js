import React, { Component } from 'react';
import GridRow from 'arui-feather/grid-row';
import TableData from './tableData';
import Head from '../heading';
import Pagination from '../pagination';

class Table extends Component {
  constructor() {
    super();
    this.sorted = { name: true };
    this.state = {
      arr: [],
      sorted: true,
      searchQuery: '',
      currentPage: 1,
      disabledPrev: true,
      disabledNext: false
    };
  }

  componentDidMount() {
    this.props.data.sort((a, b) => a.name.localeCompare(b.name)); // сортировка данных при сборке компонента
    this.setState({ arr: [...this.props.data] });
  }

  /**
   * сортировка данных
   */
  sort = (type) => {
    const { sorted, arr } = this.state;
    if (sorted) {
      arr.sort((a, b) => b[type].localeCompare(a[type]));
      this.setState({ sorted: false });
    } else {
      arr.sort((a, b) => a[type].localeCompare(b[type]));
      this.setState({ arr });
      this.setState({ sorted: true });
    }
  };

  /**
   * изменение видимости колонок таблицы
   */
  handleChangeVisibility = (isChecked, type) => {
    this.setState((prevState) => ({
      tableProp: prevState.tableProp.map((el) =>
        el.type === type
          ? el.visibility === true
            ? { ...el, visibility: false }
            : { ...el, visibility: true }
          : el
      )
    }));
  };

  handleChange = (e, name) => {
    const value = e;
    this.setState({ [name]: value });
  };

  /**
   * события клика пагинации
   */
  handleClick = (event) => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  };

  handlePrev = () => {
    const { currentPage } = this.state;
    currentPage > 1
      ? this.setState({ currentPage: currentPage - 1, disabledPrev: false })
      : this.setState({ disabledPrev: true, disabledNext: false });
  };

  handleNext = (event) => {
    const { currentPage } = this.state;
    let lastPage = Number(event.target.closest('li').id);
    lastPage >= currentPage + 1
      ? this.setState({ currentPage: currentPage + 1, disabledNext: false })
      : this.setState({ disabledNext: true, disabledPrev: false });
  };

  render() {
    const { tableProp, title, rowsCont } = this.props;
    const {
      arr,
      sorted,
      searchQuery,
      currentPage,
      disabledPrev,
      disabledNext
    } = this.state;

    /**
     * отображение текущих данных
     */
    const indexOfLastRow = currentPage * rowsCont;
    const indexOfFirstRow = indexOfLastRow - rowsCont;
    const currentData = arr.slice(indexOfFirstRow, indexOfLastRow);

    /**
     * поиск по данным на текущей странице
     */
    const filtered = currentData.filter((val) => {
      let values = Object.values(val).toString(); // возвращает все значения объекта и преобразует в String
      if (values.toLowerCase().search(searchQuery) !== -1) {
        return true;
      }
      return false;
    });

    return (
      <GridRow style={{ margin: '0 6px' }}>
        <div style={{ width: '100%' }}>
          <Head handleChange={this.handleChange} title={title} />

          <TableData
            filtered={filtered}
            tableProp={tableProp}
            sort={this.sort}
            sorted={sorted}
          />
          <Pagination
            arr={arr}
            rowsPerPage={rowsCont}
            currentPage={currentPage}
            disabledPrev={disabledPrev}
            disabledNext={disabledNext}
            handleClick={this.handleClick}
            handlePrev={this.handlePrev}
            handleNext={this.handleNext}
          />
        </div>
      </GridRow>
    );
  }
}

export default Table;
