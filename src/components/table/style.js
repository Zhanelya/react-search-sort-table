import styled from 'styled-components';
import vars from '../vars';

const Table = styled.table`
  width: 100%;
  border-spacing: 0;
  margin: 1em 0;
  > thead > tr {
    color: ${vars.color.dark_indigo};
    > th {
      text-align: left;
      border-bottom: 1px solid ${vars.color.dark_indigo_30};
    }
  }
  > tbody {
    > tr {
      > td {
        padding: 0.5em 0.75em;
        border-bottom: 1px solid ${vars.color.dark_indigo_03_deprecated};
      }
      &:nth-child(even) {
        background: ${vars.color.dark_indigo_05_flat};
      }
      &:hover {
        background: ${vars.color.dark_indigo_05_flat};
        transition: 0.2s;
      }
    }
  }
`;

const Th = styled.th`
  padding: 0.7em;
  font-weight: 600;
  ${(props) =>
    props.visibility === 'true' ? `visibility: visible` : `visibility: hidden`};
`;

const Td = styled.td`
  padding: 0.5em 0.75em;
  &:nth-child(1) {
    /* background: #ff0; */
  }
`;

export { Table, Th, Td };
