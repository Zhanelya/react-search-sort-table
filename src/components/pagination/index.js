import React from 'react';
import { PagList, PagItem } from './style';

const Pagination = (props) => {
  const {
    rowsPerPage,
    arr,
    disabledPrev,
    disabledNext,
    handleClick,
    handlePrev,
    handleNext,
    currentPage
  } = props;

  // отображение нумерации страниц
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(arr.length / rowsPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map((number) => {
    return (
      <PagItem
        key={number}
        id={number}
        onClick={handleClick}
        active={`${currentPage === number ? true : false}`}>
        {number}
      </PagItem>
    );
  });

  return (
    <PagList id="page-numbers">
      <PagItem
        className="page-arr"
        onClick={handlePrev}
        disabled={disabledPrev}>
        <span>&#8592;</span>
      </PagItem>
      {renderPageNumbers}
      <PagItem
        className="page-arr"
        id={`${pageNumbers[pageNumbers.length - 1] || 1}`}
        disabled={disabledNext}
        onClick={handleNext}>
        <span>&#8594;</span>
      </PagItem>
    </PagList>
  );
};

export default Pagination;
