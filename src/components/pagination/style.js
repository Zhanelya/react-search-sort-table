import styled, { css } from 'styled-components';
import vars from '../vars';

const PagList = styled.ul`
  list-style: none;
  display: flex;
  float: right;
  > li:hover {
    border: 1px solid ${vars.color.dark_indigo};
  }
`;

const PagItem = styled.li`
  margin-right: 0.3em;
  user-select: none;
  cursor: pointer;
  width: ${36 / 16}em;
  height: ${36 / 16}em;
  text-align: center;
  border: 1px solid ${vars.color.dark_indigo_50_flat_deprecated};
  border-radius: 4px;
  color: ${vars.textColor.dark_indigo_95};
  line-height: 2;
  font-size: 1em;
  transition: 0.2s;
  ${(props) =>
    props.active === 'true' &&
    css`
      background-color: ${vars.color.dark_indigo_05_flat};
    `};
  &.page-arr {
    > span {
      font-size: 1.3em;
      line-height: 1.5;
    }
    ${(props) =>
      props.disabled &&
      css`
        background-color: ${vars.color.dark_indigo_05_flat};
        border-color: ${vars.color.dark_indigo_03};
        color: ${vars.color.dark_indigo_03};
        cursor: no-drop;
      `};
  }
`;

export { PagList, PagItem };
