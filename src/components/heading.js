import React, { Fragment } from 'react';
import Input from 'arui-feather/input';
import Heading from 'arui-feather/heading';
import GridRow from 'arui-feather/grid-row';
import GridCol from 'arui-feather/grid-col';

const Head = (props) => {
  return (
    <Fragment>
      {props.title && (
        <GridCol width="11">
          <Heading className="my-12" size="s">
            {props.title}
          </Heading>
        </GridCol>
      )}
      <GridRow>
        <GridCol width="2">
          <Input
            placeholder="Поиск"
            name="searchQuery"
            clear={true}
            size="m"
            onChange={(e) => props.handleChange(e, 'searchQuery')}
          />
        </GridCol>
      </GridRow>
    </Fragment>
  );
};

export default Head;
