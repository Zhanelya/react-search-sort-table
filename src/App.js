import React from 'react';
import Main from './container/main';

function App() {
  return <Main />;
}

export default App;
