const data = [
  { name: 'Test data 1', id: '1', code: '123', bin: '123' },
  { name: 'Test data 2', id: '2', code: '456', bin: '456' },
  { name: 'Test data 3', id: '3', code: '789', bin: '789' },
  { name: 'Test data 4', id: '4', code: '345', bin: '345' }
];

export default data;
