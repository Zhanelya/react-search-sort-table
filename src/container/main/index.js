import React, { useState } from 'react';
import data from '../../data';
import Table from '../../components/table';

const Main = (props) => {
  // названия и типы столбцов таблицы
  const [tableProp, setTableProp] = useState([
    { name: 'Наименование клиента', type: 'name', visibility: true },
    { name: 'ID Клиента', type: 'id', visibility: true },
    { name: 'Код клиента АБИС', type: 'code', visibility: true },
    { name: 'БИН', type: 'bin', visibility: true },
    { name: 'Подразделение', type: 'id', visibility: true },
    { name: 'Сегмент', type: 'id', visibility: true },
    { name: 'Менеджер', type: 'id', visibility: true },
    { name: 'Статус', type: 'id', visibility: true }
  ]);
  return (
    <Table
      data={data} // данные в таблице
      tableProp={tableProp} // названия столбцов
      title="React Table Component" // заголовок
      rowsCont={2} // количество строк на одной странице
    />
  );
};
export default Main;
