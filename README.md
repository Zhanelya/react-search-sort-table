# react-search-sort-table

![demo gif](public/table.png)

- стиль [arui-feather](https://digital.alfabank.ru/components/amount)
- Поиск
- Сортировка
- Пагинация

## Запустить проект локально

```
git clone https://gitlab.com/Zhanelya/react-search-sort-table.git
cd *foldername*
npm install
npm start
```
